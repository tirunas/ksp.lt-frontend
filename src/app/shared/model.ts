import { IModelParams } from './interfaces/model/params.interface';
import { ModelRelations } from './interfaces/model/relations.interface';

export class Model {
  public static PK = 'id';
  public static CREATED_AT = 'createdAt';
  public static UPDATED_AT = 'updatedAt';
  public static DELETED_AT = 'deletedAt';

  protected params: IModelParams;
  protected relations: ModelRelations;

  /**
   * Get model param
   * @param {string} name
   * @returns {T}
   */
  getParam<T>(name: string): T {
    return this.params[name];
  }

  /**
   * Set model param
   * @param {string} name
   * @param {T} val
   */
  setParam<T>(name: string, val: T) {
    this.params[name] = val;
  }

  /**
   * Get model relation
   * @param {string} name
   * @returns {T}
   */
  getRelation<T>(name: string): T {
    return this.relations[name];
  }

  /**
   * Set model relation
   * @param {string} name
   * @param {T} val
   */
  setRelation<T>(name: string, val: T) {
    this.relations[name] = val;
  }

  // Main model getter setter
  get id(): number {
      return this.getParam<number>(Model.PK);
  }
  set id(value: number) {
      this.setParam<number>(Model.PK, value);
  }
  get createdAt(): string {
      return this.getParam<string>(Model.CREATED_AT);
  }
  set createdAt(value: string) {
      this.setParam<string>(Model.CREATED_AT, value);
  }
  get updatedAt(): string {
      return this.getParam<string>(Model.UPDATED_AT);
  }
  set updatedAt(value: string) {
      this.setParam<string>(Model.UPDATED_AT, value);
  }
  get deletedAt(): string {
      return this.getParam<string>(Model.DELETED_AT);
  }
  set deletedAt(value: string) {
      this.setParam<string>(Model.DELETED_AT, value);
  }
}
