import { IModelParams } from '../model/params.interface';
import { IModelTimestamps } from '../model/timestamps.interface';

export interface IGetUsers extends IModelParams, IModelTimestamps {
  name: string,
  surname: string,
  email: string,
}
