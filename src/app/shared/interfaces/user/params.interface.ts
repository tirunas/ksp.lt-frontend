import { IModelParams } from '../model/params.interface';
import { IModelTimestamps } from '../model/timestamps.interface';

export interface IUserParams extends IModelParams, IModelTimestamps {
  name: string,
  surname: string,
  email: string
}
