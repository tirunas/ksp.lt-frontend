export interface IModelTimestamps {
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
