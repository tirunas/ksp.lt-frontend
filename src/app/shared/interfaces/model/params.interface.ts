import { IModelBase } from './base.interface';
import { IModelTimestamps } from './timestamps.interface';

export interface IModelParams extends IModelBase, IModelTimestamps {}
