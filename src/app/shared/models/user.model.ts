import { Model } from '../model';
import { IUserParams } from '../interfaces/user/params.interface';
import { IUserRelations } from '../interfaces/user/relations.interface';

export class User extends Model {
  public static P_NAME: string = 'name';
  public static P_SURNAME: string = 'surname';
  public static P_EMAIL: string = 'email';

  protected params: IUserParams;
  protected relations: IUserRelations;

  get name(): string {
      return this.getParam<string>(User.P_NAME);
  }
  set name(value: string) {
      this.setParam<string>(User.P_NAME, value);
  }
  get surname(): string {
      return this.getParam<string>(User.P_SURNAME);
  }
  set surname(value: string) {
      this.setParam<string>(User.P_SURNAME, value);
  }
  get email(): string {
      return this.getParam<string>(User.P_EMAIL);
  }
  set email(value: string) {
      this.setParam<string>(User.P_EMAIL, value);
  }
}
