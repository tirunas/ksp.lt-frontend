import { Provider } from '../provider';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/user.model';
import { HttpParams } from '@angular/common/http';
import { IGetUsers } from '../interfaces/user/get.interface';

@Injectable()
export class UserProvider extends Provider {
  public static URL: string = 'users';

  // get(params?: HttpParams): Observable<User[]> {
  //   this.http.get(UserProvider.URL, params).subscribe((response: IGetUsers[]) => this._hydrateCollection<User, IGetUsers[]>(response));
  // }
}
