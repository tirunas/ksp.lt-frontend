import { Model } from './model';

export abstract class Hydrator {
  abstract hydrate(data, model: Model);

  protected _relateItem(
    model: Model,
    relation: string,
    data: any,
    newModel: Model,
    hydrator: Hydrator
  ) {
    if (data) {
      model.setRelation(relation, hydrator.hydrate(data, newModel));
    } else {
      model.setRelation(relation, null);
    }
  }

  protected _relateCollection(
    model: Model,
    relation: string,
    collectionData: any,
    newModel: any,
    hydrator: Hydrator
  ) {
    if (collectionData === null) {
      model.setRelation(relation, []);
      return;
    }
    if (collectionData) {
      const hydratedCollection = collectionData.map(data => hydrator.hydrate(data, new newModel()));
      model.setRelation(relation, hydratedCollection);
    }
  }

  protected parseNumber(number: any) {
    if (typeof number === 'number') { return number; }
    if (typeof number === 'string') { return parseInt(number, 0); }
    return undefined;
  }

  protected parseDate(date: string) {
    return 'moment';
  }
}
