import { Hydrator } from '../hydrator';
import { User } from '../models/user.model';
import { IGetUsers } from '../interfaces/user/get.interface';
import { IUserParams } from '../interfaces/user/params.interface';

export class UserHydrator extends Hydrator {
  hydrate(data: IGetUsers, model: User) {
    let params: IUserParams|any = {};
    params[User.PK] = data.id;
    params[User.P_NAME] = data.name;
    params[User.P_SURNAME] = data.surname;
    params[User.P_EMAIL] = data.email;
    params[User.CREATED_AT] = data.createdAt;
    params[User.UPDATED_AT] = data.updatedAt;

    Object.assign(model, params);

    return model
  }
}
