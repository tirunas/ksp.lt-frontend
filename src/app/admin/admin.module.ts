import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './pages/home/home.component';
import { RouterModule } from '@angular/router';
import { ADMIN_ROUTES } from './admin.routes';
import { LoginComponent } from './pages/login/login.component';

@NgModule({
  declarations: [
    AdminComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(ADMIN_ROUTES)
  ],
  exports: [
    AdminComponent,
    RouterModule
  ]
})
export class AdminModule { }
