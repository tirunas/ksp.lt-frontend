import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientComponent } from './client.component';
import { HomeComponent } from './pages/home/home.component';
import { RouterModule } from '@angular/router';
import { CLIENT_ROUTES } from './client.routes';
import { HeaderComponent } from './template/header/header.component';

@NgModule({
  declarations: [
    ClientComponent,
    HomeComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(CLIENT_ROUTES)
  ],
  exports: [
    ClientComponent,
    RouterModule
  ]
})
export class ClientModule { }
